//
//  ListenManager.h
//  p07_cui
//
//  Created by SHILEI CUI on 4/30/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ListenManager : NSObject

+ (instancetype)shareManager;

@property (assign, nonatomic) BOOL useMaxVal;


- (void)startListenWithBlock:(void(^)(Float32 level))aBlock;


- (void)stopListen;

@end
