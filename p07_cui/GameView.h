//
//  GameView.h
//  p07_cui
//
//  Created by SHILEI CUI on 4/20/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>
@import SpriteKit;

@interface GameView : SKScene

@property (assign, nonatomic) BOOL showTest;

- (void)gameOverBlock:(void(^)())aBlock;

@property (strong, nonatomic) NSString * bit8ImageText;

- (instancetype)initWithSize:(CGSize)size manImage:(NSString *)image;

@end
