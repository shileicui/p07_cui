//
//  ListenManager.m
//  p07_cui
//
//  Created by SHILEI CUI on 4/30/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "ListenManager.h"
#import "Listener.h"

static ListenManager * sss;

@interface ListenManager ()
{
    void(^_levelBlock)(Float32);
}

@property (strong, nonatomic) NSTimer * timer;
@property (strong, nonatomic) Listener * listener;

@end

@implementation ListenManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        _listener = [Listener sharedListener];
    }
    return self;
}

+ (instancetype)shareManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sss = [[ListenManager alloc] init];
    });
    return sss;
}

- (void)startListenWithBlock:(void(^)(Float32))aBlock {
    if (_levelBlock != aBlock) {
        _levelBlock = aBlock;
    }
    [_listener listen];
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.01f target:self selector:@selector(getPower) userInfo:nil repeats:YES];
}

- (void)stopListen {
    [_listener stop];
    [_timer invalidate];
    _timer = nil;
}

- (void)getPower {
    AudioQueueLevelMeterState * levels = [_listener levels];
    
    if (levels) {
        Float32 peak = 0;
        if (_useMaxVal) {
            peak = levels->mPeakPower;
        } else {
            peak = levels->mAveragePower;
        }
        
        if (levels) {
            if (_levelBlock) {
                _levelBlock(peak);
            }
        }
    }
    
}

@end

