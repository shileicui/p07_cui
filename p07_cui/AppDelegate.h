//
//  AppDelegate.h
//  p07_cui
//
//  Created by SHILEI CUI on 4/20/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
            AVAudioPlayer *audioPlayer;
}

@property (strong, nonatomic) UIWindow *window;


@end

