//
//  GameView.m
//  p07_cui
//
//  Created by SHILEI CUI on 4/20/17.
//  Copyright © 2017 scui5. All rights reserved.
//


#import "GameView.h"
#import "ListenManager.h"
#define MOVEVECTOR 50
#define LOADSPACE 60
#define LOADHEIGHT 100

static int32_t bit8Mask = 0x1 << 0;
static int32_t loadMask = 0x1 << 1;

@interface GameView ()<SKPhysicsContactDelegate>
{
    
        SKSpriteNode * bit8;
        NSMutableArray <SKSpriteNode *>* loadsArr;
        BOOL canJump;
        void(^_gameOverBlock)();
        ListenManager * manager;
        CGFloat currentVector;
    }

@end

@implementation GameView
{
    NSArray * testArr;
}

- (instancetype)initWithSize:(CGSize)size manImage:(NSString *)image
{
    self = [super initWithSize:size];
    if(self){
        _bit8ImageText = image;
        manager = [ListenManager shareManager];
        [self creatAll];
        [manager startListenWithBlock:^(Float32 level) {
            [self checkGameOver];
            [self handleActionWithVoiceLevel:level];
        }];
    }
    return self;
}

- (void)creatAll{
    [self setup];
    
    [self creatLoadWithRect:CGRectMake(0, 0, self.size.width, LOADHEIGHT)];
    
    [self creat8BitNode];
    
    //[self creatTestButton];
}

- (void)setup {
    self.physicsWorld.gravity = CGVectorMake(0, -9.8);
    self.physicsWorld.contactDelegate = self;
    self.backgroundColor = [UIColor whiteColor];
    loadsArr = [NSMutableArray array];
    
    manager = [ListenManager shareManager];
    [manager startListenWithBlock:^(Float32 level) {}];
    canJump = YES;
    currentVector = 1000;
}

- (void)testLevel:(Float32)level {
        CGPoint point = CGPointMake(202.5, self.size.height-50);
        SKSpriteNode * testNode = testArr[2];
        testNode.position = CGPointMake(point.x, point.y+level*testNode.size.height);
    }


- (void)creatTestButton {
        SKSpriteNode * jump = [SKSpriteNode spriteNodeWithColor:[UIColor orangeColor] size:CGSizeMake(50, 50)];
        [self addChild:jump];
        [jump setPosition:CGPointMake(50, self.size.height-50)];
    
        SKSpriteNode * move = [SKSpriteNode spriteNodeWithColor:[UIColor purpleColor] size:CGSizeMake(50, 50)];
        [self addChild:move];
        [move setPosition:CGPointMake(150, self.size.height-50)];
        SKSpriteNode * level = [SKSpriteNode spriteNodeWithColor:[UIColor greenColor] size:CGSizeMake(20, 50)];
        [self addChild:level];
        [level setPosition:CGPointMake(200, self.size.height-50)];
        SKSpriteNode * level2 = [SKSpriteNode spriteNodeWithColor:[UIColor whiteColor] size:CGSizeMake(25, 50)];
        [self addChild:level2];
        [level2 setPosition:CGPointMake(202.5, self.size.height-50)];
    

        SKSpriteNode * vectorA = [SKSpriteNode spriteNodeWithColor:[UIColor purpleColor] size:CGSizeMake(25, 25)];
        [self addChild:vectorA];
        [vectorA setPosition:CGPointMake(250, self.size.height-50-25)];

        SKLabelNode * vectorShow = [SKLabelNode labelNodeWithText:[NSString stringWithFormat:@"%.0lf", currentVector]];
        vectorShow.fontName = [UIFont systemFontOfSize:15].fontName;
        vectorShow.fontSize = 13;
        vectorShow.fontColor = [UIColor blackColor];
        [self addChild:vectorShow];
        [vectorShow setPosition:CGPointMake(250+25+50/2, self.size.height-50-25-25/2)];

        SKSpriteNode * vectorR = [SKSpriteNode spriteNodeWithColor:[UIColor purpleColor] size:CGSizeMake(25, 25)];
        [self addChild:vectorR];
        [vectorR setPosition:CGPointMake(250+25+50+25/2, self.size.height-50-25)];
    
        testArr = @[jump, move, level2, @[vectorA, vectorR, vectorShow]];
    }


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch * tou = [touches anyObject];
    CGPoint po = [tou locationInNode:self];
    SKSpriteNode * node = testArr[0];
    SKSpriteNode * node2 = testArr[1];
    SKSpriteNode * vA = testArr[3][0];
    SKSpriteNode * vR = testArr[3][1];
    SKLabelNode * vS = testArr[3][2];
    
    if ([node containsPoint:po]) {
        [self moveLoad];
    } else if ([node2 containsPoint:po]) {
        [self jump8Bit];
    } else if ([vA containsPoint:po]) {
        currentVector += 10;
        vS.text = [NSString stringWithFormat:@"%.0lf", currentVector];
    } else if ([vR containsPoint:po]) {
        currentVector -= 10;
        vS.text = [NSString stringWithFormat:@"%.0lf", currentVector];
    }
}

- (void)creat8BitNode {
        bit8 = [SKSpriteNode spriteNodeWithImageNamed:_bit8ImageText?_bit8ImageText:@"bit8"];
        [bit8 setSize:CGSizeMake(30, 30)];
        [bit8 setPosition:CGPointMake(self.size.width/3, self.size.height-25)];
        [self addChild:bit8];
        bit8.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:bit8.size];
        bit8.physicsBody.mass = 10;
        bit8.physicsBody.categoryBitMask = bit8Mask;
        bit8.physicsBody.contactTestBitMask = loadMask;
    }


- (void)creatLoadWithRect:(CGRect)rect {
        SKSpriteNode * load = [SKSpriteNode spriteNodeWithColor:[UIColor lightGrayColor] size:rect.size];
        [self addChild:load];
        load.position = CGPointMake(rect.origin.x+rect.size.width/2, rect.origin.y+rect.size.height/2);
        load.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:rect.size];
        load.physicsBody.dynamic = NO;
        load.physicsBody.categoryBitMask = loadMask;
        load.physicsBody.contactTestBitMask = bit8Mask;
        [loadsArr addObject:load];
    }


- (NSTimeInterval)timeWithHowLong:(CGFloat)howLong {
        return [self timeWithHowLong:howLong vector:MOVEVECTOR];
    }


- (NSTimeInterval)timeWithHowLong:(CGFloat)howLong vector:(CGFloat)vector {
        return howLong/vector;
    }

- (void)handleActionWithVoiceLevel:(Float32)level {
    const Float32 runLevel = 0.1f;
    const Float32 jumpLevel = 0.5f;
    if (level > runLevel) {
        if (level > jumpLevel) {
            [self jump8Bit:level];
            CGFloat jumpLong = (LOADSPACE+bit8.size.width)*level;
            [self moveLoadWithLong:jumpLong vector:jumpLong/10*MOVEVECTOR];
        } else {
            [self moveLoad];
        }
    }
    
}

- (void)moveLoad {
    [self moveLoadWithLong:10];
}

- (void)moveLoadWithLong:(CGFloat)loadLong {
    [self moveLoadWithLong:loadLong vector:MOVEVECTOR];
}

- (void)moveLoadWithVector:(CGFloat)vector {
    [self moveLoadWithLong:10 vector:vector];
}

- (void)moveLoadWithLong:(CGFloat)loadLong vector:(CGFloat)vector {
    
        SKSpriteNode * lastLoad = [loadsArr lastObject];
        if (self.size.width-(lastLoad.position.x+lastLoad.size.width/2) > LOADSPACE) {
                [self creatLoadWithRect:CGRectMake(lastLoad.position.x+lastLoad.size.width/2+LOADSPACE, 0, arc4random()%200+50, LOADHEIGHT)];
            }
    
        SKSpriteNode * firstLoad = [loadsArr firstObject];
        if (firstLoad.position.x+firstLoad.size.width/2 < 0) {
                [firstLoad removeFromParent];
                [loadsArr removeObjectAtIndex:0];
            }
    
    [loadsArr enumerateObjectsUsingBlock:^(SKSpriteNode * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj runAction:[SKAction moveToX:obj.position.x-loadLong duration:[self timeWithHowLong:loadLong vector:vector]]];
    }];
}


- (void)jump8Bit {
    [self jump8Bit:1.0f];
}


- (void)jump8Bit:(Float32)level {
        if(canJump){
                bit8.physicsBody.velocity = CGVectorMake(0, currentVector*level);
                canJump = NO;
            }
}

- (void)didBeginContact:(SKPhysicsContact *)contact {
        if (((contact.bodyA.categoryBitMask == bit8Mask)&&(contact.bodyB.categoryBitMask == loadMask)) || ((contact.bodyB.categoryBitMask == bit8Mask)&&(contact.bodyA.categoryBitMask == loadMask))) {
                if (bit8.position.y > bit8.size.height/2+LOADHEIGHT) {
                       canJump = YES;
                    }
    }
}

- (void)checkGameOver {
    if (bit8.position.y < -bit8.size.height) {
        [[ListenManager shareManager] stopListen];
        if (_gameOverBlock) {
            _gameOverBlock();
        }
    }
}

- (void)gameOverBlock:(void (^)())aBlock {
    if (_gameOverBlock != aBlock) {
        _gameOverBlock = aBlock;
    }
}

@end
