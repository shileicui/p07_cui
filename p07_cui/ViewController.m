//
//  ViewController.m
//  p07_cui
//
//  Created by SHILEI CUI on 4/20/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "ViewController.h"
#import "GameView.h"
@import SpriteKit;

@interface ViewController ()
{
    SKView * gameView;
    NSString * manImageName;
}
@property (weak, nonatomic) IBOutlet UIButton *startButton;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
}

- (IBAction)startGame:(id)sender {
    [self resetGame];
}

- (void)resetGame {
    gameView = [[SKView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:gameView];
    GameView * scene = [[GameView alloc] initWithSize:gameView.frame.size manImage:manImageName];
    [scene gameOverBlock:^{
        [gameView removeFromSuperview];
        gameView = nil;
    }];
    [gameView presentScene:scene];
}
@end

