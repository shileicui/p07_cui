//
//  Listener.h
//  p07_cui
//
//  Created by SHILEI CUI on 4/30/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioQueue.h>
#import <AudioToolbox/AudioServices.h>

@interface Listener : NSObject {
    AudioQueueLevelMeterState *levels;
    
    AudioQueueRef queue;
    AudioStreamBasicDescription format;
    Float64 sampleRate;
}

+ (Listener *)sharedListener;

- (void)listen;
- (BOOL)isListening;
- (void)pause;
- (void)stop;

- (Float32)averagePower;
- (Float32)peakPower;
- (AudioQueueLevelMeterState *)levels;

@end
